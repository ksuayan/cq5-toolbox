package com.globalbin.services;

import java.util.Dictionary;

public interface ConfigurationProviderService {
    public Dictionary<String,String> getConfig();
}
