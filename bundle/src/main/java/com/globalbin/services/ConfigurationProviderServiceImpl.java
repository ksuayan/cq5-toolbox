package com.globalbin.services;

import java.util.Dictionary;
import java.util.Hashtable;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(immediate=true, metatype=true, label="GB Site Configuration Component")
@Service(ConfigurationProviderService.class)
@Properties({ 
    @Property(name="service.description", value="Site Configuration Provider Service"),
    @Property(name="service.vendor", value="GlobalBin.com")
})
public class ConfigurationProviderServiceImpl implements ConfigurationProviderService {
    
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    
    @Property(value="This is a console configurable value.", description="Sample configurable value") 
    private static final String SAMPLE_CONFIG = "sample.config";
    
    private Dictionary<String,String> config = new Hashtable<String,String>();
    
    @Activate
    protected void activate(ComponentContext componentContext) {
        log.info("SiteConfigurationComponent: Activated.");
        setup(componentContext.getProperties());
    }
    
    @Deactivate
    protected void deactivate(ComponentContext componentContext) {
        log.info("SiteConfigurationComponent: Deactivated.");
    }

    protected void setup(Dictionary<String, String> properties) {
        if (this.config!=null) {
            this.config.put(SAMPLE_CONFIG, properties.get(SAMPLE_CONFIG).toString());
        }
    }
    
    public Dictionary<String,String> getConfig() {
        return this.config;
    }
}
