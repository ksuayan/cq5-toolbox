package com.globalbin.tags;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Enumeration;
import javax.servlet.jsp.JspException;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.scripting.SlingBindings;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.globalbin.services.ConfigurationProviderService;


@Component(immediate=true, metatype=true, label="GB Site Configuration Tag")
public class ConfigTag extends BaseTagSupport {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    
    private static final long serialVersionUID = 630403587941120155L;
    
    @Reference
    SlingScriptHelper slingScriptHelper;
    
    
    @Activate
    protected void activate(ComponentContext componentContext) {
        log.info("ConfigTag: Activated." + componentContext.toString());
    }
    
    @Deactivate
    protected void deactivate(ComponentContext componentContext) {
        log.info("ConfigTag: Deactivated.");
    }
    
    @Override
    public int doStartTag() throws JspException {
        slingRequest = getSlingRequest();
        SlingBindings bindings = (SlingBindings) slingRequest.getAttribute(SlingBindings.class.getName());
        SlingScriptHelper scriptHelper = bindings.getSling();
        out = getOut();
                
        ConfigurationProviderService siteConfig 
            = scriptHelper.getService(com.globalbin.services.ConfigurationProviderService.class);
        
        Dictionary<String, String> config = siteConfig.getConfig();
        
        JSONObject jsonObj = new JSONObject();
        for (Enumeration<String> e = config.keys(); e.hasMoreElements();) {
            String key = e.nextElement().toString(); 
            try {
                jsonObj.put(key, config.get(key));
            } catch (JSONException e1) {
                log.error(e1.getMessage());
            }            
         }
        
        try {
            out.println(jsonObj.toString());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return SKIP_BODY;
    }
}
