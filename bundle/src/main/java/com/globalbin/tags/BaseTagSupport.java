package com.globalbin.tags;

import org.apache.sling.api.SlingHttpServletRequest;
import javax.jcr.Node;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class BaseTagSupport extends TagSupport {

    private static final long serialVersionUID = -5730777227953514646L;
    protected SlingHttpServletRequest slingRequest;
    protected JspWriter out;
    protected String currentPage;
    protected Node currentPageNode;

    public SlingHttpServletRequest getSlingRequest() {
        return (SlingHttpServletRequest) this.pageContext.getRequest();
    }

    public JspWriter getOut() {
        return pageContext.getOut();
    }

    public void setCurrentPage(String paramCurrentPage) throws JspException {
        this.currentPage = paramCurrentPage;
    }

    public String getCurrentPage() {
        if (currentPage != null) {
            return currentPage;
        } else {
            return  this.getSlingRequest().getResource().getPath();
        }
    }

    public Node getCurrentPageNode() {
        return this.getSlingRequest().getResource().adaptTo(Node.class);
    }

}
