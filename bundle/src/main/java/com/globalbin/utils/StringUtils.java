package com.globalbin.utils;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringUtils {
    private static final Logger log = LoggerFactory.getLogger(StringUtils.class);
    private StringUtils() {}
    public static <T> boolean contains( final T[] array, final T v ) {
        for ( final T e : array )
            if ( e == v || v != null && v.equals( e ) )
                return true;
        return false;
    }
    
    /**
     * Check if a value v startsWith() an element in an array
     * @param array
     * @param v
     * @return
     */
    public static boolean startsWith( final String[] array, final String v ) {
        for ( final String e : array )
            if ( v != null && v.startsWith(e) )
                return true;
        return false;
    }

    /**
     * Simple string join. 
     * @param values
     * @param joinStr
     * @return
     */
    public static String join(List<String>values, String joinStr ) {
        StringBuilder sb = new StringBuilder();
        if (values == null||values.size()==0) {
            return "";
        }
        for (int i=0,n=values.size(); i<n; i++) {
            String value = values.get(i);
            if (value!=null) {
                sb.append(value);
                if (joinStr!=null && i<n-1)
                    sb.append(joinStr);
            }
        }
        return sb.toString();
    }
    
    /**
     * Fix improperly spaced semicolons.
     * @param text
     * @return
     */
    public static String fixSemicolons(String text) {
        return text.replaceAll("(\\w+):(\\w+)", "$1: $2");
    }

}
