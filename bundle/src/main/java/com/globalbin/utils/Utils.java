package com.globalbin.utils;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utils {

	private static final Logger log = LoggerFactory.getLogger(Utils.class);
	public static final int NODE_LIMIT = 20;
    
    private Utils() {        
    }
        
    /**
     * Put the value into the JSONObject if it's not null.
     * @param jso
     * @param key
     * @param value
     * @return
     */
    public static JSONObject checkedPut(JSONObject jso, String key, String value) {
        if (jso == null) 
            return jso;
        if (value!=null) {
            try {
                jso.put(key, value);
            } catch (JSONException e) {
            }
        }
        return jso;
    }
    
    /**
     * Attach a list to a jsonObject.
     * @param jso
     * @param key
     * @param list
     * @return
     */
    public static JSONObject checkedPut(JSONObject jso, String key, List<String> list) {
        if (jso==null || list==null)
            return jso;
        if (list.size()>0) {
            JSONArray jlist = new JSONArray();
            for (String value : list) {
                jlist.put(value);
            }
            try {
                jso.put(key, jlist);
            } catch (JSONException e) {
            }
        }
        return jso;
    }
    
    /**
     * Create a JSONArray from relPath with excluded attributes.
     * @param node
     * @param relPath
     * @param excludes
     * @return
     */
    public static JSONArray getArrayFromNodes(Node node, String relPath, String[] excludes){
    	JSONArray jsa = null;
    	NodeIterator ni;
		try {
			ni = node.getNode(relPath).getNodes();
	    	if (ni!= null && ni.getSize()>0) {
	    		int count = 0;
	    		jsa = new JSONArray();
	    		while(ni.hasNext() && count<NODE_LIMIT ){
	    			Node child = ni.nextNode();
	    			JSONObject obj = new JSONObject();
	    			obj = attachProperties(obj, child, excludes);
	    			jsa.put(obj);
	    		}
	    	}
		} catch (RepositoryException e) {
		}
    	return jsa;
    }

    /**
     * Given a set of keys, concatenate the properties of the JSONObject obj with joinStr.
     * @param obj
     * @param keys
     * @param joinStr
     * @return
     */
    public static String concatProperties(JSONObject obj, String[] keys, String joinStr ) {
    	StringBuilder sb = new StringBuilder();
    	if (obj == null) {
    		return "";
    	}
    	for (int i=0,n=keys.length; i<n; i++) {
    		try {
        		String value = obj.getString(keys[i]);
        		if (value!=null) {
        			sb.append(value);
            		if (joinStr!=null)
            			sb.append(joinStr);
        		}
    		} catch (JSONException e) {
    		}
    	}
    	return sb.toString();
    }

    /**
     * Attach JSON attributes from node's relative path
     * @param obj
     * @param node
     * @param relPath
     * @return
     */
    public static JSONObject attachSubProperties(JSONObject obj, Node node, String relPath, String[] excludes ) {
    	try {
        	Node childNode = node.getNode(relPath);
    		obj = attachProperties(obj, childNode, excludes);
    	} catch (RepositoryException e) {
		}
    	return obj;
    }
    
    /**
     * JSONObject decorator that attaches node properties to a json object.
     * @param obj
     * @param node
     * @param excludes
     * @return
     */
    public static JSONObject attachProperties(JSONObject obj, Node node, String[] excludes) {
    	try {
        	PropertyIterator pi = node.getProperties();
        	if (pi!=null && pi.getSize()>0) {
        		while (pi.hasNext()){
        			javax.jcr.Property p = pi.nextProperty();
        			String propName = p.getName();
        			if (!StringUtils.startsWith(excludes, propName)) {
               			if (p.isMultiple()) {
            				obj.put(propName, getMultiPropertyArray(p));
            			} else {
            				obj.put(propName, p.getValue().getString());
            			}
        			}
        		}
        	}    		
		} catch (RepositoryException e) {
		} catch (JSONException e) {
		} 
    	return obj;
    }
    
    /**
     * Retrieve multi-prop values as JSONArray.
     * @param resourceResolver
     * @param path
     * @param subpath
     * @param propertyName
     * @return
     */
    public static JSONArray getMultiPropertyJsonArray(ResourceResolver resourceResolver, 
            String path, String subpath, String propertyName) {
        
        JSONArray jsonArray = null;        
        Node node = Utils.getNode(resourceResolver, path, subpath);        
        if (node == null) {
            log.info(">>> node not found ... ["+path+"]["+subpath+"]["+propertyName+"]");
            return null;
        }
        try {
            Property prop = node.getProperty(propertyName);
            jsonArray = new JSONArray();            
            if (prop!=null) {
                if (prop.isMultiple()) {
                    Value[] valueList = prop.getValues();                    
                    for (int i=0, n=valueList.length; i<n; i++) {
                        JSONObject jsonObj = new JSONObject(valueList[i].getString());
                        jsonArray.put(i, jsonObj);
                    }                   
                } else {
                    JSONObject jsonObj = new JSONObject(prop.getString());
                    jsonArray.put(0, jsonObj);
                }
            } 
        } catch (JSONException e) {
        } catch (ValueFormatException e) {
        } catch (IllegalStateException e) {
        } catch (PathNotFoundException e) {
        } catch (RepositoryException e) {
        }
        return jsonArray;
    }
    
    /**
     * Return a json array from a multi-value property
     * @param p
     * @return
     */
    public static JSONArray getMultiPropertyArray(javax.jcr.Property p) {
    	JSONArray jsa = null;
    	Value[] values;
		try {
			values = p.getValues();
			jsa = new JSONArray();
	    	for (int i=0,n=values.length; i<n; i++){
	    		jsa.put(values[i].getString());
	    	}
		} catch (ValueFormatException e) {
		} catch (RepositoryException e) {
		}
    	return jsa;
    }
    
    /**
     * Retrieve property prop from Node selected via path.
     * @param path
     * @param prop
     * @param session
     * @return
     */
    public static String getProperty(String path, String prop, Session session) {
    	String result = null;
        try {
            Node node = session.getNode(path);
            if (node != null) {
                Property property = node.getProperty(prop);
                if (property != null) {
                    return property.getValue().getString();
                }
            }
        } catch (RepositoryException e) {
        }
        return result;
    }
    
    /**
     * Retrieve property from node with given subpath (key).
     * @param node
     * @param subpath
     * @return
     */
    public static String getPropertyString(Node node, String subpath) {
    	if (node == null)
    		return null;
    	
    	String value = null;    	
    	try {
			value = node.getProperty(subpath).getValue().getString();
		} catch (ValueFormatException e) {
		} catch (IllegalStateException e) {
		} catch (PathNotFoundException e) {
		} catch (RepositoryException e) {
		}
    	return value;
    }

    /**
     * Retrieve multi-properties as ArrayList from node with given subpath (key).
     * @param node
     * @param subpath
     * @return
     */
    public static ArrayList<String> getPropertyStringArray(Node node, String subpath) {
    	if (node == null)
    		return null;
    	ArrayList<String> values = null;
    	try {
    		Property prop = node.getProperty(subpath);
    		if (prop!=null) {
    			if (prop.isMultiple()) {
    	    		Value[] valueList = prop.getValues();
        			values = new ArrayList<String>(valueList.length);
        			for (int i=0, n=valueList.length; i<n; i++) {
        				values.add(valueList[i].getString());
        			}
    			} else {
    				values = new ArrayList<String>();
    				values.add(prop.getString());
    			}
    		}    		
		} catch (ValueFormatException e) {
		} catch (IllegalStateException e) {
		} catch (PathNotFoundException e) {
		} catch (RepositoryException e) {
		}
    	return values;
    }
    
    public static Node getNode(String path, Session session) {
    	Node node = null;
        try {
            node = session.getNode(path);
            if (node!=null) {
            	return node;
            }
        } catch(RepositoryException re) {
        }
        return node;
    }
    
    public static String[] getAttributesFromJsonMulti(ResourceResolver resourceResolver, 
            String path, String subpath, String propName, String jsonAttribute) {
        
        if (subpath == null || propName == null || jsonAttribute == null)
            return new String[]{null};

        Node node = Utils.getNode(resourceResolver, path, subpath);
        if (node==null)
            return new String[]{null};

        ArrayList<String> propArray = Utils.getPropertyStringArray(node, propName);
        if (propArray == null) {
            return new String[]{null};
        }
        int numProps = propArray.size();
        String[] values = new String[numProps];
        for (int i = 0; i < numProps; i++) {
            String propStr = propArray.get(i);
            if (propStr != null && propStr.trim().length() > 0) {
                try {
                    JSONObject jsonObj = new JSONObject(propStr);
                    if (jsonObj != null) {
                        values[i] = jsonObj.getString(jsonAttribute);
                    }
                } catch (JSONException e) {
                    values[i] = null;
                }
            }
        }
        return values;
    }
    
    /**
     * Get Node relative to this resource's node.
     * @param subpath
     * @return
     */
    public static Node getNode(ResourceResolver resourceResolver, String path, String subpath) {
        String resourcePath = path + "/jcr:content/" + subpath;
        return Utils.getNode(resourceResolver, resourcePath);
   }
    
    /**
     * Overloaded.
     * @param resourceResolver
     * @param resourcePath
     * @return
     */
    public static Node getNode(ResourceResolver resourceResolver, String resourcePath) {
        Resource resource = resourceResolver.getResource(resourcePath);
        if (resource != null)
            return resource.adaptTo(Node.class);
        else
            return null;
    }
    
    
    /**
     * Create an ArrayList of Text list given a subpath and a selected property name.
     * @param subpath
     * @param propName
     * @return
     */
    public static ArrayList<String> getCompositeTextList(ResourceResolver resourceResolver, String path, String subpath, String[] propNames) {
        
        if (propNames == null || propNames.length == 0)
            return null;
        
        ArrayList<String> textList = new ArrayList<String>();
        try {
            Node node = Utils.getNode(resourceResolver, path, subpath);
            if (node != null) {
                NodeIterator ni = node.getNodes();
                
                while (ni.hasNext()) {
                    Node subnode = (Node) ni.next();
                    StringBuffer sb = new StringBuffer();
                    
                    // concatenate selected properties values.
                    for (int i=0,n=propNames.length; i<n; i++) {                        
                        String value = subnode.getProperty(propNames[i]).getValue().getString();
                        if (value!=null && value.trim().length()>0) {
                            sb.append(value);
                            if (i+1<n)
                                sb.append(", ");                            
                        }
                    }
                    textList.add(StringUtils.fixSemicolons(sb.toString()));
                }               
            }
        } catch (RepositoryException re) {
        }
        return textList;
    }
    
}
