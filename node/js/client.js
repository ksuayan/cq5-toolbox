var http = require('http');

var options = {
    host: 'localhost',
    port: 4502,
    path: '/content/cq5-training/english/about.html',
    headers: {
        'Authorization': 'Basic ' + new Buffer("admin:admin").toString('base64')
    }
};

request = http.get(options, function(res){
    var body = "";
    res.on('data', function(data) {
        body += data;
    });
    res.on('end', function() {
        console.log(body);
    })
    res.on('error', function(e) {
        console.log("Got error: " + e.message);
    });
});